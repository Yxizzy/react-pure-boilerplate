Get Started:

yarn install
yarn start

To build:
yarn run build

To test:
yarn test

For eslint fix:
yarn eslint-fix